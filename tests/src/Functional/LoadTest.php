<?php

namespace Drupal\Tests\micro_menu\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\micro_site\Functional\MicroSiteBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group micro_menu
 */
class LoadTest extends MicroSiteBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'micro_menu',
    'micro_site',
    'micro_node',
    'menu_link_content',
    'menu_ui',
    'content_translation',
    ];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The theme to install as the default for testing.
   *
   * Defaults to the install profile's default theme, if it specifies any.
   *
   * @var string
  */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testLoad() {
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->assertSession()->statusCodeEquals(200);
  }

}
